import axios from "axios";

export default axios.create({
  baseURL: "http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/curso",
  headers: {
    "Content-type": "application/json"
  }
});