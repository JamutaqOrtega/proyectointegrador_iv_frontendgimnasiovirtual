import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.bundle'

import './App.css';

import Home from './components/home.component';
import Nav from './components/nav.component';
import Login from './components/login.component';
import Registro from './components/registro.component';
import Cursos from './components/cursos.component'
import Perfil from './components/perfil.component'

import {BrowserRouter, Route, Switch} from 'react-router-dom'
import ActualizarDatos from './components/actualizardatos.component';
import ActualizarCuenta from './components/actualizarcuenta.component';
import Recarga from './components/recarga.component';
import LoginProfesor from './components/loginProfesor.component';
import RegistroProfesor from './components/registroProfesor.component';
import PerfilProfesor from './components/perfilProfesor.component';
import CrearCurso from './components/crearCurso.component';
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Nav />
        <div className="auth-wrapper">
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/loginProfesor" component={LoginProfesor}/>
              <Route exact path="/registro" component={Registro}/>
              <Route exact path="/registroProfesor" component={RegistroProfesor}/>
              <Route exact path="/cursos" component={Cursos}/>
              <Route exact path="/perfil" component={Perfil}/>
              <Route exact path="/perfilProfesor" component={PerfilProfesor}/>
              <Route exact path="/actualizarDatos" component={ActualizarDatos}/>
              <Route exact path="/actualizarCuenta" component={ActualizarCuenta}/>
              <Route exact path="/agregarSaldo" component={Recarga}/>
              <Route exact path="/crearCurso" component={CrearCurso}/>
            </Switch>
        </div>
      </div>

    </BrowserRouter>
  );
}

export default App;
