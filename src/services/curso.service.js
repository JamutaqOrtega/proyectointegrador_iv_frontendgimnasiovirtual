import http from "../http-common";

class CursoService {

    getAll(params) {
        return http.get("/listaPage", { params });
    }

}

export default new CursoService();