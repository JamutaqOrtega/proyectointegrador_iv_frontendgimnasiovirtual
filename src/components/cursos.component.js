import React, { Component } from 'react'
import Skeleton from '@yisheng90/react-loading';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const cookies = new Cookies()


export default class Cursos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cursos: [],
            recuperado: false,
            abierto: false
        }
    }

    componentWillMount() {
        fetch('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/curso/lista')
            .then((response) => {
                return response.json()

            }).then((cur) => {
                this.setState({ cursos: cur })
                this.setState({
                    cursos: cur,
                    recuperado: true
                })
            })
    }

    comprarCurso(id_curso, id_est) {
        axios.put(`http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/curso/matricula/${id_curso}/${id_est}`)
            .then(response => {
                window.location.reload()
                alert(response.data.mensaje)

            }, err => {
                toast.error(err.response.data.mensaje)
            }
            )
    }

    mostrarModal() {
        this.setState({ abierto: !this.state.abierto }) //cambia el estado a true

        return (
            <div>
                <Modal isOpen={this.state.abierto}>
                    <ModalHeader>Detalle del curso</ModalHeader>
                    <ModalBody>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.setState({ abierto: false })}>Aceptar</Button>{' '}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

    mostrarTabla() {
        return (
            <div className="listado-inner tex-center">
                <h3>Listado de cursos</h3>
                <ToastContainer />
                {this.state.cursos.map(cur => {
                    return (
                        <div>
                            <div className="card text-center">
                                <div className="card-header">
                                    Salud
                                    </div>
                                <div className="card-body">
                                    <h5 className="card-title">{cur.nombre}</h5>
                                    <p className="card-text">{cur.descripcion} <br /> <b>Precio: </b>{cur.precio}</p>
                                    <Link href="#" className="btn btn-primary" onClick={() => this.mostrarModal()}>Ver detalles</Link>
                                    {"   "}
                                    <button className="btn btn-success" onClick={() => this.comprarCurso(cur.id, cookies.get('id'))}>Comprar</button>
                                </div>
                                <div className="card-footer text-muted">
                                    {cur.sesiones} sesiones - {cur.capacidad} vacantes
                            </div>
                            </div>
                            <br />
                        </div>
                    );
                })}

            </div>
        );
    }
    render() {
        if (this.state.recuperado)
            return this.mostrarTabla()
        else
            return (
                <div className="listado-inner">
                    <h2 className="text-center">Recuperando datos...</h2>
                    <br />
                    <Skeleton height={25} rows={5} />
                </div>

            );
    }
}
