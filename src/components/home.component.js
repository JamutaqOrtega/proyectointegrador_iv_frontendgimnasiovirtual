import React, { Component } from 'react'
import Cookies from 'universal-cookie'
import "react-toastify/dist/ReactToastify.css"
import Carrusel from './carrusel.component'

const cookies = new Cookies()

export default class Home extends Component {

    

    render() {
        if (!cookies.get('username')) { //si no existe la cookie username, que redirija al login
            return (
                <div className="auth-inner">
                    <h2>No has iniciado sesión</h2>
                </div>
            )
        }
        return (
            <div className="listado-inner text-center">
                <h2 className=" mb-3">Bienvenido {cookies.get('nombre')} {cookies.get('apellido')}</h2>
                <Carrusel/>
                
            </div>

        )
    }
}
