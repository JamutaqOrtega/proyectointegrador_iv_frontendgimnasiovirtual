import React, { Component } from 'react'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"
import Cookies from 'universal-cookie'

const cookies = new Cookies()

export default class Recarga extends Component {

    handleSubmit = e => {
        e.preventDefault()
        const data = {
            saldo: this.saldo,
        }
        
        axios.put('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/estudiante/agregarSaldo/' + cookies.get('id'), data)
        .then(
            res=>{
                
                window.location.href = './'
                alert(res.data.mensaje)
            }, err=>{
                toast.error(err.response.data.mensaje)   
            }
        )
    }

    render() {
        return (
            <div className="auth-inner">
                <form onSubmit={this.handleSubmit}>
                    <ToastContainer/>
                    <h3>Recarga de saldo</h3>

                    <div className="form-group mb-3">
                        <label>Cantidad</label>
                        <input type="number" className="form-control" placeholder="Cantidad" onChange={e => this.saldo = e.target.value}></input>
                    </div>

                    <button className="btn btn-success col-12">Agregar saldo</button>
                </form>
            </div>
        )
    }
}
