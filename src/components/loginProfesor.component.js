import axios from 'axios';
import React, { Component } from 'react'
import { ToastContainer, toast } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"
import Cookies from 'universal-cookie'

const url = "http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/profesor/login"
const cookies = new Cookies() //instancia la clase

export default class LoginProfesor extends Component {

    state = {
        //atributos para el login
        form: {
            username: '',
            password: ''
        }
    }

    //metodo para capturar los valores de los inputs
    //para verlo e tiempo real que sea async
    handleChange = async e => {
        await this.setState({
            //se guarda en el estado el valor del input de acuerdo a su nombre
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
    }

    handleSubmit = async e => {
        e.preventDefault()
        let credenciales = {
            username: this.state.form.username,
            password: this.state.form.password,
        }

        await axios.post(url, credenciales)
            .then(response => { //retorna la data
                if (response.data !== '') { //se inicio sesion de forma correcta, guardar cookies
                    cookies.set('id', response.data.id, { path: "/" }) //establecer cookies
                    cookies.set('nombre', response.data.nombre, { path: "/" })
                    cookies.set('apellido', response.data.apellido, { path: "/" })
                    cookies.set('username', response.data.username, { path: "/" })
                    cookies.set('correo', response.data.correo, { path: "/" })
                    cookies.set('rol', 'profesor', { path: "/" })

                    //muesta alerta
                    alert("Aunteticación correcta")

                    //redirige al menu
                    window.location.href = './'

                } else {
                    toast.error("El usuario o la contraseña son incorrectas")

                }
            }, err => {
                toast.error(err.response.data.mensaje)
            })
    }



    render() {
        return (
            <div className="auth-inner">
                <form onSubmit={this.handleSubmit}>
                    <ToastContainer />
                    <h3>Iniciar sesión<h6><span className="badge bg-secondary">Profesor</span></h6></h3>

                    <div className="form-group mb-3">
                        <label>Nombre de usuario</label>
                        <input type="text" className="form-control" name="username" placeholder="Nombre de usuario" onChange={this.handleChange}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Contraseña</label>
                        <input type="password" className="form-control" name="password" placeholder="Contraseña" onChange={this.handleChange}></input>
                    </div>

                    <button className="btn btn-primary col-12">Iniciar sesión </button>
                </form>
            </div>
        )
    }
}
