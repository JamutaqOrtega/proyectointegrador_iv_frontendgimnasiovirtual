import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Cookies from 'universal-cookie'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignInAlt, faUserPlus, faUser, faShoppingCart, faFire, faCoins, faPlusCircle, faBookOpen } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';

const cookies = new Cookies()
const API_URL = 'http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/estudiante/perfil/id/'

export default class Nav extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saldo: 0,
            calorias_perdidas: 0.0
        }
    }

    async componentWillMount() {
        if (cookies.get('id')){
            axios.get(API_URL + cookies.get('id')).then(res => {
                this.setState({
                    saldo: res.data.saldo,
                    calorias_perdidas: res.data.calorias_perdidas
                })
                console.log(res.data.saldo)
            })
        }else{
            return
        }
        
    }

    render() {
        if (cookies.get('rol') === 'profesor') { //si ha iniciado como profesor
            return (
                <nav className="navbar navbar-expand navbar-light fixed-top">
                    <div className="container">
                        <Link to={'/'} className="navbar-brand"> ViGym <span className="badge bg-warning">Profesor</span></Link>

                        <div className="collapse navbar-collapse">
                            <ul className="navbar-nav ms-auto">
                                <li className="nav-item">
                                    <Link to={'/perfilProfesor'} className="nav-link"><FontAwesomeIcon icon={faUser} /> Mi perfil</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={'/cursos'} className="nav-link"><FontAwesomeIcon icon={faBookOpen} /> Mis cursos</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={'/crearCurso'} className="nav-link"><FontAwesomeIcon icon={faPlusCircle} /> Crear curso</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            )
        } else if (cookies.get('rol') === 'estudiante') { //si ha iniciado como estudiante
            return (
                <nav className="navbar navbar-expand navbar-light fixed-top">
                    <div className="container">
                        <Link to={'/'} className="navbar-brand"> ViGym <span className="badge bg-primary">Estudiante</span></Link>

                        <div className="collapse navbar-collapse">
                            <ul className="navbar-nav ms-auto">
                                <li className="nav-item">
                                    <Link to={'/perfil'} className="nav-link"><FontAwesomeIcon icon={faUser} /> Mi perfil</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={'/cursos'} className="nav-link"><FontAwesomeIcon icon={faShoppingCart} /> Comprar Cursos</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link"><FontAwesomeIcon icon={faFire} /> Calorías: {this.state.calorias_perdidas}</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to={'/agregarSaldo'} className="nav-link"><FontAwesomeIcon icon={faCoins} /> Saldo: {this.state.saldo}</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            )
        } else {
            return (
                <nav className="navbar navbar-expand navbar-light fixed-top">
                    <div className="container">
                        <Link to={'/'} className="navbar-brand"> ViGym </Link>
                        <div className="collapse navbar-collapse">
                            <ul className="navbar-nav ms-auto">
                                <li className="nav-item">
                                    <div className="collapse navbar-collapse">
                                        <ul className="navbar-nav">
                                            <li className="nav-item dropdown">
                                                <Link to="/#" className="nav-link dropdown-toggle" id="loginE" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <FontAwesomeIcon icon={faSignInAlt} /> Iniciar sesión
                                                </Link>
                                                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                                    <li><Link to={'/login'} className="dropdown-item">Como estudiante</Link></li>
                                                    <li><Link to={'/loginProfesor'} className="dropdown-item">Como profesor</Link></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <div className="collapse navbar-collapse" >
                                        <ul className="navbar-nav">
                                            <li className="nav-item dropdown">
                                                <Link to="/#" className="nav-link dropdown-toggle" id="loginE" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <FontAwesomeIcon icon={faUserPlus} /> Registrarse
                                                </Link>
                                                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                                    <li><Link to={'/registro'} className="dropdown-item">Como estudiante</Link></li>
                                                    <li><Link to={'/registroProfesor'} className="dropdown-item">Como profesor</Link></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            )
        }

    } 
}
