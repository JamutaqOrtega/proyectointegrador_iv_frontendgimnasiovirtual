import React, { Component } from 'react'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"

export default class Registro extends Component {

    handleSubmit = e => {
        e.preventDefault()
        const data = {
            nombre: this.nombre,
            apellido: this.apellido,
            username: this.username,
            correo: this.correo,
            password: this.password
        }
        
        axios.post('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/estudiante/registrar', data)
        .then(
            res=>{
                
                window.location.href = './login'
                alert(res.data.mensaje)
            }, err=>{
                toast.error(err.response.data.mensaje)   
            }
        )
    }

    render() {
        return (
            <div className="auth-inner">
                <form onSubmit={this.handleSubmit}>
                    <ToastContainer/>
                    <h3>Registro<h6><span className="badge bg-secondary">Estudiante</span></h6></h3>

                    <div className="form-group mb-2">
                        <label>Nombre</label>
                        <input type="text" className="form-control" placeholder="Nombre" onChange={e => this.nombre = e.target.value}></input>
                    </div>
                    <div className="form-group mb-2">
                        <label>Apellido</label>
                        <input type="text" className="form-control" placeholder="Apellido" onChange={e => this.apellido = e.target.value}></input>
                    </div>
                    <div className="form-group mb-2">
                        <label>Username</label>
                        <input type="text" className="form-control" placeholder="Username" onChange={e => this.username = e.target.value}></input>
                    </div>
                    <div className="form-group mb-2">
                        <label>Correo</label>
                        <input type="email" className="form-control" placeholder="Correo" onChange={e => this.correo = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Contraseña</label>
                        <input type="password" className="form-control" placeholder="Contraseña" onChange={e => this.password = e.target.value}></input>
                    </div>

                    <button className="btn btn-primary col-12">Crear cuenta</button>
                </form>
            </div>
        )
    }
}
