import React, { Component } from 'react'
import Skeleton from '@yisheng90/react-loading';
import Cookies from 'universal-cookie'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAt, faCalendar, faEnvelope, faFont, faUserClock, faUserGraduate } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom';

const cookies = new Cookies()

export default class PerfilProfesor extends Component {
    constructor(props) {
        super(props);

        this.state = {
            datos: {},
            recuperado: false
        }
    }

    cerrarSesion = () => {
        //borra las variables de sesion
        cookies.remove('id', { path: "/" })
        cookies.remove('nombre', { path: "/" })
        cookies.remove('apellido', { path: "/" })
        cookies.remove('username', { path: "/" })
        cookies.remove('correo', { path: "/" })
        cookies.remove('rol', { path: "/" })
        window.location.href = './'
    }

    componentWillMount() {
        fetch('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/profesor/perfil/username/' + cookies.get('username'))
            .then((response) => {
                return response.json()

            }).then((data) => {
                this.setState({ datos: data })
                this.setState({
                    datos: data,
                    recuperado: true
                })
                console.log(data)
                console.log(this.state.datos)
            })
    }

    mostrarDatos() {
        return (
            <div className="perfil-inner">
                <h3>Mi perfil</h3>
                <br />
                <div className="card">
                    <h5 className="card-header text-center">Datos de la cuenta</h5>
                    <div className="card-body">
                        <p>
                            <b><FontAwesomeIcon icon={faFont} /> Nombres y apellidos: </b>
                            {this.state.datos.nombre} {this.state.datos.apellido} 
                        </p>
                        <p>
                            <b><FontAwesomeIcon icon={faAt} /> Nombre de usuario: </b>
                            {this.state.datos.username}
                        </p>
                        <p>
                            <b><FontAwesomeIcon icon={faEnvelope} /> Correo: </b>
                            {this.state.datos.correo}
                        </p>
                        <Link className="btn btn-primary col-12" to={'./actualizarCuenta'}>Actualizar datos</Link>
                    </div>
                </div>
                <br />
                <div className="card">
                    <h5 className="card-header text-center">Datos adicionales</h5>
                    <div className="card-body">
                        <p>
                            <b><FontAwesomeIcon icon={faUserGraduate} /> Ocupación: </b>
                            {this.state.datos.ocupacion}
                        </p>
                        <p>
                            <b><FontAwesomeIcon icon={faUserClock} /> Experiencia: </b>
                            {this.state.datos.experiencia} años
                        </p>
                        <p>
                            <b><FontAwesomeIcon icon={faCalendar} /> Edad: </b>
                            {this.state.datos.edad} años
                        </p>
                        <Link className="btn btn-primary col-12" to={'./actualizarDatos'}>Actualizar datos</Link>
                    </div>
                </div>
                <br/>
                <div className="text-center">
                    <button className="btn btn-danger" onClick={() => this.cerrarSesion()}>Cerrar sesión</button>
                </div>
                
            </div>
        );
    }

    render() {
        if (this.state.recuperado) //true
            return this.mostrarDatos() //muestra la tabla
        else //si es falso (aun no llega los datos)
            return (
                <div className="auth-inner">
                    <h2 className="text-center">Recuperando datos...</h2>
                    <br />
                    <Skeleton height={25} rows={5} />
                </div>

            );
    }
}
