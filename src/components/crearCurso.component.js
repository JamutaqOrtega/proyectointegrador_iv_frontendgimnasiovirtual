import React, { Component } from 'react'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"

export default class CrearCurso extends Component {

    handleSubmit = e => {
        e.preventDefault()
        const data = {
            nombre: this.nombre,
            descripcion: this.descripcion,
            sesiones: this.sesiones,
            capacidad: this.capacidad,
            precio: this.precio,
            calorias_perdidas: this.calorias_perdidas
        }
        
        axios.post('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/curso/crear', data)
        .then(
            res=>{                
                window.location.href = './cursos'
                alert(res.data.mensaje)
            }, err=>{
                toast.error(err.response.data.mensaje)   
            }
        )
    }

    render() {
        return (
            <div className="auth-inner">
                <form onSubmit={this.handleSubmit}>
                    <ToastContainer/>
                    <h3>Crear nuevo curso</h3>

                    <div className="form-group mb-2">
                        <label>Nombre</label>
                        <input type="text" className="form-control" placeholder="Nombre" onChange={e => this.nombre = e.target.value}></input>
                    </div>
                    <div className="form-group mb-2">
                        <label>Descripción</label>
                        <textarea className="form-control" placeholder="Descripción" onChange={e => this.descripcion = e.target.value} rows="3"></textarea>
                    </div>
                    <div className="form-group mb-2">
                        <label>Número de sesiones</label>
                        <input type="number" className="form-control" placeholder="Número de sesiones" onChange={e => this.sesiones = e.target.value}></input>
                    </div>
                    <div className="form-group mb-2">
                        <label>Capacidad de alumnos</label>
                        <input type="number" className="form-control" placeholder="Capacidad de alumnos" onChange={e => this.capacidad = e.target.value}></input>
                    </div>
                    <div className="form-group mb-2">
                        <label>Precio</label>
                        <input type="number" className="form-control" placeholder="Precio" onChange={e => this.precio = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Calorías que se perderán con este curso</label>
                        <input type="number" className="form-control" placeholder="Calorías" onChange={e => this.calorias_perdidas = e.target.value}></input>
                    </div>

                    <button className="btn btn-primary col-12">Crear curso</button>
                </form>
            </div>
        )
    }
}
