import React, { Component } from 'react'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"
import Cookies from 'universal-cookie'

const cookies = new Cookies() 

export default class ActualizarDatos extends Component {

    handleSubmit = e => {
        e.preventDefault()
        const data = {
            estatura: this.estatura,
            peso: this.peso,
        }
        
        axios.put('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/estudiante/actualizar/datos/' + cookies.get('id'), data)
        .then(
            res=>{
                window.location.href = './perfil'
                alert(res.data.mensaje)
            }, err=>{
                toast.error(err.response.data.mensaje)   
            }
        )
    }

    render() {
        return (
            <div className="auth-inner">
                <form onSubmit={this.handleSubmit}>
                    <ToastContainer/>
                    <h3>Actualizar datos físicos</h3>

                    <div className="form-group mb-3">
                        <label>Estatura</label>
                        <input type="number" className="form-control" placeholder="Estatura" onChange={e => this.estatura = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Peso</label>
                        <input type="number" className="form-control" placeholder="Peso" onChange={e => this.peso = e.target.value}></input>
                    </div>
                    <button className="btn btn-primary col-12">Actualizar</button>
                </form>
            </div>
        )
    }
}
