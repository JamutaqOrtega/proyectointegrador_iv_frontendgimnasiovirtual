import React, { Component } from 'react'
import axios from 'axios'
import { ToastContainer, toast } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css"
import Cookies from 'universal-cookie'

const cookies = new Cookies() 

export default class ActualizarCuenta extends Component {

    handleSubmit = e => {
        e.preventDefault()
        const data = {
            nombre: this.nombre,
            apellido: this.apellido,
            username: this.username,
            correo: this.correo,
            password: this.password,
        }
        
        axios.put('http://pigoshvigymivtecsup-env.eba-7mmzkvdh.us-east-2.elasticbeanstalk.com/vigym/api/estudiante/actualizar/cuenta/' + cookies.get('id'), data)
        .then(
            res=>{
                alert(res.data.mensaje)
                this.cerrarSesion()
            }, err=>{
                toast.error(err.response.data.mensaje)   
            }
        )
    }

    cerrarSesion = () => {
        //borra las variables de sesion
        cookies.remove('id', { path: "/" })
        cookies.remove('id', { path: "/" })
        cookies.remove('nombre', { path: "/" })
        cookies.remove('apellido', { path: "/" })
        cookies.remove('username', { path: "/" })
        cookies.remove('correo', { path: "/" })
        cookies.remove('estatura', { path: "/" })
        cookies.remove('peso', { path: "/" })
        cookies.remove('imc', { path: "/" })
        cookies.remove('calorias_perdidas', { path: "/" })
        
        window.location.href = './login'
    }

    render() {
        return (
            <div className="auth-inner">
                <form onSubmit={this.handleSubmit}>
                    <ToastContainer/>
                    <h3>Actualizar datos de la cuenta</h3>

                    <div className="form-group mb-3">
                        <label>Nombre</label>
                        <input type="text" className="form-control" placeholder="Nombre" onChange={e => this.nombre = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Apellido</label>
                        <input type="text" className="form-control" placeholder="Apellido" onChange={e => this.apellido = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Nombre de usuario</label>
                        <input type="text" className="form-control" placeholder="Nombre de usuario" onChange={e => this.username = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Correo</label>
                        <input type="email" className="form-control" placeholder="Correo" onChange={e => this.correo = e.target.value}></input>
                    </div>
                    <div className="form-group mb-3">
                        <label>Contraseña</label>
                        <input type="password" className="form-control" placeholder="Contraseña" onChange={e => this.password = e.target.value}></input>
                    </div>
                    <button className="btn btn-primary col-12" >Actualizar</button>
                </form>
            </div>
        )
    }
}
